# Frequently Asked Questions

Here are frequently asked questions about your submission data, payment methods and the premium plans:

## Can anyone access my forms and my submission data?

Your forms and form submission data are only accessible by authorized users on your account and user accounts you invited to your forms. All the data you submit to Forminia is strictly protected with our GDPR compatible data protection policies.

## Can I cancel my premium plan anytime?

Yes! There are no bounds. It’s easy to downgrade your account from any premium package to free one or in between premium plans. Please note that after we receive your downgrade request, you will still keep your premium package rights & features until the next billing period.

## Is 100 submissions count of the free plan for one time or reset every month?

100 submissions count of free plan is for one time and doesn't reset every month.

## How can I upgrade my account to premium?

We offer 3 different premium packages organized around different levels of needs. You can select the one that fits your needs and by using your credit card you can start your premium subscription. Package upgrades will be effected immediately after the successful payment.

## How can I get the submission data collected by my form?

We store each form submission for viewing via our form details dashboard and offer an export option in the .csv format that can be opened using most major spreadsheet programs including Excel, Apple Numbers and Google Docs.

## How do you store my credit card information?

We use [Stripe](https://stripe.com) to securely store your card, easily and securely enable payment features in our services.

## Do you block spam?

Yes, we use [Akismet](https://akismet.com) and it's Machine Learning techniques to block the spam submissions and keep your forms spam free! We also support Google reCAPTCHA v2 and v3 which you can setup in your forms. See here for more info on the reCAPTCHA setup.

## Can I change my email address?

Yes, you can change your email address after the registration. In order to change your email address, you should send your change request to [talk@forminia.com](talk@forminia.com) from the email address you wish to change to.
